//
//  main.m
//  Cottages of College Station
//
//  Created by Cody Richards on 6/29/15.
//  Copyright (c) 2017 Communique, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMQAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CMQAppDelegate class]));
    }
}
